import React from 'react';
import Container from '@material-ui/core/Container';

import AppBar from '../AppBar';
import ProTip from '../ProTip';
import Copyright from '../Copyright';

export default function Layout({ children }) {
    return (
        <Container maxWidth="sm">
            <AppBar/>
            {children}
            <ProTip/>
            <Copyright/>
        </Container>
    );
}
