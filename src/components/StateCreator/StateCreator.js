import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchStates } from '../../state/slices/states';

import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import SaveIcon from '@material-ui/icons/Save';
import ClearIcon from '@material-ui/icons/Clear'

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import { createState } from '../../state/slices/states';

export default function StateCreator() {

    const dispatch = useDispatch();
    const initialState = { name: '', acronym: '', createdAt: new Date() };
    const [values, setValues] = useState(initialState);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({ ...values, [name]: value });
    };

    const handleCreateState = async () => {
        await dispatch(createState(values));
        await dispatch(fetchStates());
        handleInputClear();
    };

    const handleInputClear = () => {
        setValues({...initialState});
    };

    return (
        <Box my={2}>
            <Card>
                <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                        Cadastro de estado
                    </Typography>
                    <form>
                        <TextField
                            name="name"
                            label="Nome"
                            fullWidth
                            onChange={handleInputChange}
                            margin="dense"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            value={values.name}
                            required
                        />
                        <TextField
                            name="acronym"
                            label="Sigla"
                            fullWidth
                            onChange={handleInputChange}
                            margin="dense"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            value={values.acronym}
                            required
                        />
                    </form>
                </CardContent>
                <CardActions>
                    <Button variant="contained" color="primary" startIcon={<SaveIcon/>} onClick={handleCreateState}>
                        Cadastrar
                    </Button>
                    <Button variant="contained" startIcon={<ClearIcon/>} onClick={handleInputClear}>
                        Limpar campos
                    </Button>
                </CardActions>
            </Card>
        </Box>
    )
}
