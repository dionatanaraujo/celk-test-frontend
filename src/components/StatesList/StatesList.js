import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { deleteState } from '../../state/slices/states';

import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

export default function StatesList() {
    const dispatch = useDispatch();

    const classes = useStyles();
    const { states } = useSelector((state) => state.states);

    const handleDeleteState = (id) => dispatch(deleteState(id));

    return (
        <Box my={2}>
            <Paper elevator={3}>
                <List className={classes.root} subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                        Estados cadastrados
                    </ListSubheader>
                }>
                {states && states.map(state =>
                    <ListItem key={state.id} button>
                        <ListItemIcon>
                            <Avatar alt="Remy Sharp">{state.acronym}</Avatar>
                        </ListItemIcon>
                        <ListItemText primary={state.name}/>
                        <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteState(state.id)}>
                                <DeleteIcon/>
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>)}
                </List>
            </Paper>
        </Box>
    );
}
