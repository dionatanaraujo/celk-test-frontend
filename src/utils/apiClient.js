import axios from 'axios';

const baseURL = process.env.NODE_ENV === 'production'
    ? 'https://daraujo-backend-celk.herokuapp.com/api/v1'
    : 'http://localhost:8080/api/v1';

const apiClient = axios.create({
    baseURL,
});

apiClient.defaults.crossDomain = true;

export default apiClient;
