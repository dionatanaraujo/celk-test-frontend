import React from 'react';
import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import configureStore from '../state';
import theme from '../theme';

import Layout from '../components/Layout';

class Application extends React.Component {

    static getInitialProps = async (appContext) => {
        const appProps = await App.getInitialProps(appContext);
        return {...appProps};
    };

    render() {
        const {Component, pageProps, store} = this.props;
        return (
            <Provider store={store}>
                <Head>
                    <title>CELK</title>
                    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width"/>
                </Head>
                <ThemeProvider theme={theme}>
                    <CssBaseline/>
                    <Layout>
                        <Component {...pageProps} />
                    </Layout>
                </ThemeProvider>
            </Provider>
        );
    }
}

Application.propTypes = {
    Component: PropTypes.elementType.isRequired,
    pageProps: PropTypes.object.isRequired,
};

export default withRedux(configureStore)(Application);
