import React from 'react';
import StatesScreen from '../screens/States';
import { fetchStates } from '../state/slices/states';

const States = ({ ...props }) => <StatesScreen {...props} />;

States.getInitialProps = async ({ store }) => {
  const promises = [];

  promises.push( store.dispatch(fetchStates()));
  await Promise.all(promises);

  const states = store.getState().states;

  return { states };
};

States.propTypes = {};

export default States;
