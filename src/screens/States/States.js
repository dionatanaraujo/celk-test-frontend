import React from 'react';
import StateCreator from '../../components/StateCreator';
import StateList from '../../components/StatesList';

export default function States() {
    return (
        <React.Fragment>
            <StateCreator />
            <StateList/>
        </React.Fragment>
    );
}
