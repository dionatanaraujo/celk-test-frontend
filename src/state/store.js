import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';

export default (initialState) => configureStore({
    preloadedState: initialState,
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware()]
});
