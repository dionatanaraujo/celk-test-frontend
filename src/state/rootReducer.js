import { combineReducers } from '@reduxjs/toolkit';
import statesReducer from './slices/states';

const rootReducer = combineReducers({
    states: statesReducer
});

export default rootReducer;
