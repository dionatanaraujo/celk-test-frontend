import { createSlice } from '@reduxjs/toolkit';
import apiClient from '../../utils/apiClient';

const initialState = {};

const states = createSlice({
    name: 'states',
    initialState,
    reducers: {
        createStateStart(state) {
            state.isLoading = true;
        },
        createStateSuccess(state, { payload }) {
            state.states = state.states.put(payload.data);
            state.isLoading = false;
        },
        createStateFailure(state, action) {
            state.isLoading = false;
            state.error = action.payload;
        },
        deleteStateStart(state) {
            state.isLoading = true;
        },
        deleteStateSuccess(state, { payload }) {
            state.states = state.states.filter((i) => i.id !== payload.id);
            state.isLoading = false;
        },
        deleteStateFailure(state, action) {
            state.isLoading = false;
            state.error = action.payload;
        },
        getStatesStart(state) {
            state.isLoading = true;
        },
        getStatesSuccess(state, { payload }) {
            state.states = payload.data;
            state.isLoading = false;
            state.error = null;
        },
        getStatesFailure(state, action) {
            state.isLoading = false;
            state.error = action.payload;
        },
    },
});

export const {
    createStateStart,
    createStateSuccess,
    createStateFailure,
    deleteStateStart,
    deleteStateSuccess,
    deleteStateFailure,
    getStatesStart,
    getStatesSuccess,
    getStatesFailure,
} = states.actions;

export default states.reducer;

export const fetchStates = () => async (dispatch) => {
    try {
        dispatch(getStatesStart());
        const {data} = await apiClient.get('states');
        dispatch(getStatesSuccess({
            data,
        }));
    } catch (e) {
        dispatch(getStatesFailure(e.toString()));
    }
};

export const deleteState = (id) => async (dispatch) => {
    try {
        dispatch(deleteStateStart());
        await apiClient.delete(`states/${id}`);
        dispatch(deleteStateSuccess({ id }));
    } catch (e) {
        dispatch(deleteStateFailure(e.toString()));
    }
};

export const createState = (state) => async (dispatch) => {
    try {
        dispatch(createStateStart());
        const { data } = await apiClient.post('states', state);
        dispatch(createStateSuccess({
            data,
        }));
    } catch (e) {
        dispatch(createStateFailure(e.toString()));
    }
};
