## front-end

Este projeto foi desenvolvido utilizadno as tecnologias:

- SSR (Server Side Render) com Next.Js
- React.Js
- Material Design

Demostraçåo online:
https://daraujo-frontend-celk.herokuapp.com/

Intalação:

```sh
npm install
```

Excução:
```sh
npm run dev
```

[Next.js](https://github.com/zeit/next.js) is a framework for server-rendered React apps.
[React.js](https://pt-br.reactjs.org/) the UI Library
